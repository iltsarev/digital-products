//
//  DPButtonStyle.swift
//  DigitalProducts
//
//  Created by Ilya Tsarev on 22.01.17.
//  Copyright © 2017 Ilya Tsarev. All rights reserved.
//

import UIKit

extension DPButton {

    // This approach can be used in any control: cells, labels, pickers, etc.
        
    func generalStyle() {
        // Title of the button
        self.setTitle("General", for: .normal)
        
        // Background color for different states
        self.setBackgroundColor(UIColor.buttonGeneral(), forUIControlState: .normal)
        self.setBackgroundColor(UIColor.buttonGeneralSelected(), forUIControlState: .highlighted)
        
        // Title color for different states
        self.setTitleColor(UIColor.textPrimary(), for: .normal)
        
        // Corner radius. Default is zero
        self.layer.cornerRadius = 12.0
    }
    
    func otherStyle() {
        self.setTitle("Other", for: .normal)
        
        self.setBackgroundColor(UIColor.buttonDefault(), forUIControlState: .normal)
        
        self.setTitleColor(.textSecondary(), for: .normal)
        self.setTitleColor(.textPrimary(), for: .highlighted)
        
        self.layer.cornerRadius = 12.0
        
        // Border configuration
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.buttonOther().cgColor
    }
    
    func defaultStyle() {
        self.setTitle("Default", for: .normal)
        
        self.setBackgroundColor(UIColor.buttonOther(), forUIControlState: .normal)
        self.setTitleColor(UIColor.textPrimary(), for: .normal)
    }

    func imageStyle() {
        self.setTitle("Image", for: .normal)
        
        // Setting backround image
        // Image can be found in Resources/Assets.xcassets
        self.setBackgroundImage(UIImage(named: "buttonBackground"), for: .normal)
        self.setTitleColor(UIColor.textSecondary(), for: .normal)
    }
}
