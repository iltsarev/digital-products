//
//  ViewController.swift
//  DigitalProducts
//
//  Created by Ilya Tsarev on 22.01.17.
//  Copyright © 2017 Ilya Tsarev. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    let buttonWidth : Double = 200
    let buttonHeight : Double = 40
    let buttonsPadding : Double = 80
    
    lazy var upperView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.primary()
        return view
    }()

    lazy var lowerView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.secondary()
        return view
    }()
    
    lazy var firstGeneralButton : DPButton = {
        let button = DPButton.button(style: .general)
        return button
    }()
    
    lazy var secondGeneralButton : DPButton = {
        let button = DPButton.button(style: .general)
        return button
    }()
    
    lazy var thirdGeneralButton : DPButton = {
        let button = DPButton.button(style: .general)
        return button
    }()
    
    lazy var firstOtherButton : DPButton = {
        let button = DPButton.button(style: .other)
        return button
    }()

    lazy var secondOtherButton : DPButton = {
        let button = DPButton.button(style: .other)
        return button
    }()
    
    lazy var defaultButton : DPButton = {
        let button = DPButton.button()
        return button
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        setupConstraints()
    }

    //MARK: - Layout
    
    private func addSubviews() {
        // Adding upper and lower views
        view.addSubview(upperView)
        view.addSubview(lowerView)
        
        // Adding buttons
        upperView.addSubview(firstGeneralButton)
        upperView.addSubview(secondGeneralButton)
        upperView.addSubview(thirdGeneralButton)
        lowerView.addSubview(firstOtherButton)
        lowerView.addSubview(secondOtherButton)
        lowerView.addSubview(defaultButton)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupConstraints() {
        
        // Initializing constrains for view layout
        upperView.snp.makeConstraints { [unowned self] (make) in
            make.top.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(self.view.frame.size.height / 2.0)
        }
        
        lowerView.snp.makeConstraints { [unowned self] (make) in
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(self.view.frame.size.height / 2.0)
        }

        setupButtonsConstraints()
    }
    
    private func setupButtonsConstraints() {
        // Buttons on upper view
        firstGeneralButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.upperView)
            make.centerY.equalTo(self.upperView).offset(-self.buttonsPadding)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
        
        secondGeneralButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.upperView)
            make.centerY.equalTo(self.upperView)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
        
        thirdGeneralButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.upperView)
            make.centerY.equalTo(self.upperView).offset(self.buttonsPadding)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
        
        // Buttons on lower view
        firstOtherButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.lowerView)
            make.centerY.equalTo(self.lowerView).offset(-self.buttonsPadding)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
        
        secondOtherButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.lowerView)
            make.centerY.equalTo(self.lowerView)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
        
        defaultButton.snp.makeConstraints { [unowned self] (make) in
            make.centerX.equalTo(self.lowerView)
            make.centerY.equalTo(self.lowerView).offset(self.buttonsPadding)
            make.width.equalTo(self.buttonWidth)
            make.height.equalTo(self.buttonHeight)
        }
    }
}

