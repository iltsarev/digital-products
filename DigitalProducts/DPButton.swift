//
//  DPButton.swift
//  DigitalProducts
//
//  Created by Ilya Tsarev on 22.01.17.
//  Copyright © 2017 Ilya Tsarev. All rights reserved.
//

import UIKit

enum DPButtonStyle : Int {
    case `default` = 0
    case general
    case other
}

class DPButton: UIButton {

    var style : DPButtonStyle = .default
    
    //MARK: Factory
    
    class func button(style : DPButtonStyle = .default) -> DPButton {
        let button = DPButton(type: .custom)
        button.style = style
        button.configureStyle()
        button.layer.masksToBounds = true
        button.layer.shouldRasterize = true
        button.layer.rasterizationScale = UIScreen.main.scale
        return button
    }
    
    //MARK: Configuration
    
    func configureStyle() {
        switch style {
        case .general: generalStyle(); break
        case .other: otherStyle(); break
        case .default: defaultStyle(); break
        }
    }
}

extension UIButton {
    
    /// Used for ability to set backround color for button state
    private func image(with color: UIColor) -> UIImage? {
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(1.0), height: CGFloat(1.0))
        
        UIGraphicsBeginImageContext(rect.size)
        let context: CGContext? = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(_ color: UIColor, forUIControlState state: UIControlState) {
        self.setBackgroundImage(image(with: color), for: state)
    }
}
