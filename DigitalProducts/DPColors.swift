//
//  DPColors.swift
//  DigitalProducts
//
//  Created by Ilya Tsarev on 22.01.17.
//  Copyright © 2017 Ilya Tsarev. All rights reserved.
//

import UIKit

public extension UIColor {
    
    // This colors used application-wide. Change one of them and then run the project.
    
    //MARK: - Fills
    
    class func primary() -> UIColor {
        return UIColor(hex: 0x0B1F35)
    }
    
    class func secondary() -> UIColor {
        return UIColor(hex: 0x0B1F35).withAlphaComponent(0.5)
    }
    
    //MARK: - Buttons

    class func buttonDefault() -> UIColor {
        return UIColor.clear
    }
    
    class func buttonGeneral() -> UIColor {
        return UIColor(hex: 0xF03226)
    }

    class func buttonGeneralSelected() -> UIColor {
        return UIColor(hex: 0xF03226).withAlphaComponent(0.8)
    }
    
    class func buttonOther() -> UIColor {
        return UIColor(hex: 0x0B1F35).withAlphaComponent(0.3)
    }
    
    //MARK: - Text
    
    class func textPrimary() -> UIColor {
        return UIColor.white
    }
    
    class func textSecondary() -> UIColor {
        return UIColor(hex: 0x0B1F35).withAlphaComponent(0.7)
    }
}
