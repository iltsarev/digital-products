Пример проекта в рамках интенсива "Дизайн digital-продуктов" в Британской Высшей Школе Дизайна.

### Как запустить? ###

* Скачать **xcode** не ниже версии 8.2.1
* Запустить программу **Терминал**
* С помощью bash-команды cd перейти в папку с проектом [Здесь можно прочесть про команды в терминале](http://linuxgeeks.ru/bash-intro.htm)
* Вставить в командную строку и нажать Enter: */usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"*
* Вставить в командную строку и нажать Enter: *brew install carthage*
* Вставить в командную строку и нажать Enter: *carthage update*
* Открыть папку с проектом
* Запустить файл **DigitalProducts.xcodeproj**

### Что делать? ###

Код, который отвечает за стили и цвета располагается в DigitalProducts/Sources/Extensions 

* DPColors.swift отвечает за цвета в проекте
* DPButtonStyle отвечает за цвет кнопок
* [Здесь](http://www.learnswift.tips) приводятся статьи для знакомства с языком программирования swift